import Link  from 'next/link'
import Head from 'next/head'

export default function Home() {
  return (
    <div>

      <Head>
        <title>ReactJS Course</title>
      </Head>

      <h2>
        Home
      </h2>
    </div>

  )
}