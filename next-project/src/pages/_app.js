import {useState, useEffect} from 'react'
import random from 'lodash/random'
import Menu from '../components/generic/Menu'
import Header from '../components/generic/Header'
import Footer from 'cmp/generic/Footer'
import Sidebar from 'components/generic/Sidebar'
// import NextNprogress from 'nextjs-progressbar';
import { useRouter } from 'next/router'
import '../styles/global.css'

export default function MyApp ({Component, pageProps}) {

  const router = useRouter()
  const [loading, setLoading] = useState(false)

  useEffect(() =>{
    router.events.on('routeChangeStart', () => setLoading(true))
    router.events.on('routeChangeComplete', () => setLoading(false))

    return () => {
      router.events.off('routeChangeStart', () => setLoading(true))
      router.events.off('routeChangeComplete', () => setLoading(false))
    }
  }, [])

  return (
    <div>

      {/*<NextNprogress*/}
      {/*  color="#29D"*/}
      {/*  startPosition={0.3}*/}
      {/*  stopDelayMs={200}*/}
      {/*  height="3"*/}
      {/*/>*/}
      {loading && 'Loading ........'}

      {/*{_.random(0,20)}*/}
      {random(0,20)}
      <Header/>

      <section style={{display: "flex"}}>

        <Sidebar/>

        <article>
          <Component {...pageProps} />
        </article>

      </section>


      <Footer/>
    </div>
  )

}