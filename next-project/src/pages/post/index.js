import { useEffect, useState } from 'react'
import axios from 'axios'
import Link from 'next/link'
import Head from 'next/head'

export default function Posts ({posts= []}) {

  // const [posts, setPosts] = useState([])
  //
  // useEffect(() => {
  //   axios('https://jsonplaceholder.typicode.com/posts')
  //     .then(response => setPosts(response.data))
  // }, [])

  return (
    <div>
      <Head>
        <title>My post list</title>
      </Head>

      <h2>
        Post list
      </h2>
      <ol>
        {posts.map(post => <Link key={post.id} href={`/post/${post.id}`}><a><li>{post.title}</li></a></Link>)}
      </ol>
    </div>
  )
}

export async function getServerSideProps() {
  const {data: posts} = await axios('https://jsonplaceholder.typicode.com/posts')

  // console.log(hi)
  console.log(posts.length)
  return {
    props: {
      posts
    }
  }
}