import {useRouter} from 'next/router'
import Link from 'next/link'
import axios from 'axios'
import Head from 'next/head'

export default function Post({post= {}}) {

  const router = useRouter()

  console.log(post)

  return (
    <div>


      <Head>
        <title>{post.title}</title>
      </Head>

      {/*Post {router.query.id}*/}
      {post.title}
      <p>
        {post.body}
      </p>

      <hr/>

      <Link href="/post">
        <a>
          Return to list
        </a>
      </Link>

    </div>
  )
}

export async function getServerSideProps({params}) {

  const {data: post} = await axios(`https://jsonplaceholder.typicode.com/posts/${params.id}`)

  // console.log(Object.keys(context))
  // console.log(context.params.id)

  return {
    props: {
      post
    }
  }
}