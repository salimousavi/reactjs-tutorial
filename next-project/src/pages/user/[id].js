import {useRouter} from 'next/router'
import Link from 'next/link'
// import Error from 'next/error'
import Error from '../_error'
import axios from 'axios'

export default function Post({user= {}, statusCode}) {

  // console.log(user)

  if (statusCode) {
    return <Error statusCode={statusCode} />
  }

  return (
    <div>
      {user.name}
      <p>
        {user.email}
      </p>

      <hr/>

      <Link href="/user">
        <a>
          Return to list
        </a>
      </Link>

    </div>
  )
}

export async function getStaticPaths(context) {

  console.log('MYCONTEXT ===================>', context)

  const {data: users} = await axios('https://jsonplaceholder.typicode.com/users')


  users.length = 5


  return {
    paths: users.map(user => ({params: {id: `${user.id}`}})),
    fallback: true
  }
}

export async function getStaticProps({params}) {

  const res = await axios(`https://jsonplaceholder.typicode.com/users/${params.id}`).catch(err => console.log('ERROR'))

  const user = res?.data

  const statusCode = res ? false: 404

  // console.log(Object.keys(context))
  // console.log(context.params.id)

  // console.log('User id is =>>>', user.id)

  return {
    props: {
      user,
      statusCode
    },
    revalidate: 5
  }
}