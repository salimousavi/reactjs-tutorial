import Menu from './Menu'


export default function Footer() {
  return (
    <aside>
      <Menu />
    </aside>
  )
}