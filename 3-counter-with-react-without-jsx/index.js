class Counter extends React.Component{
  constructor () {
    super()
    this.state = { counter: 0 };
  }

  increaseButton = () => {
    return React.createElement(
      'button',
      {
        style: {margin: '10px'},
        onClick: () => this.setState({counter: this.state.counter + 1})
      },
      'Increase')
  }

  decreaseButton = () => {
    return React.createElement(
      'button',
      {
        onClick: () => this.setState({counter: this.state.counter - 1})
      },
      'decrease')
  }

  render() {
    return React.createElement('div',
      {},
      React.createElement('span', {}, this.state.counter), this.increaseButton(), this.decreaseButton())
  }
}

ReactDOM.render(React.createElement(Counter), document.querySelector('#root'))

