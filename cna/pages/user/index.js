import axios from 'axios'


export default function User({users}) {

  return (
    <div>
      <h2> Users list </h2>

      <ul>
        {users.map(user => <li key={user.name}>{user.name}</li>)}
      </ul>
    </div>
  )
}

export async function getServerSideProps() {

  const {data: users} = await axios('https://jsonplaceholder.typicode.com/users')

  console.log(users)
  return {
    props: {
      users
    }
  }
}

