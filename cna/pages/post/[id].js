// import { useRouter } from 'next/router'

export default function Post (props) {


  // =======> For twice render
  // this is the expected behavior for automatically statically optimized dynamic pages.
  // When a dynamic page does not leverage getServerSideProps, getStaticProps, or getInitialProps it is auto prerendered
  // at build time and since the dynamic page's values can't be known during this stage the initial HTML doesn't isn't
  // rendered with them so during the first render on the client we can't provide these values either to make sure to
  // match the initial HTML and not break hydration.
  // After hydration has occurred we parse the values from the URL on the client and trigger another render to allow
  // the parsed values to be used now that hydration has occurred.

  // console.log(props)

  // const router = useRouter()
  // const {id} = router.query

  const id = 5
  console.log('Post')
  // console.log(id, router)

  return (
    <div>Salam {id}</div>
  )

}

// Post.getInitialProps = () => {
//   return {
//     props: {
//       family: 'Mousavi'
//     },
//     name: 'SAlam'
//   }
// }

// export async function getStaticPaths() {
//   return {paths: []}
// }
//
// export async function getStaticProps() {
// export async function getServerSideProps(content) {
//   // const router = useRouter()
//   // const {id} = router.query
//
//   console.log('saaaaa')
//   console.log(Object.keys(content))
//
//   return {
//     props: {id:5}
//   }
// }
