import React from 'react'

class Binding extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      counter: 0
    }

    this.increment = this.increment.bind(this)
  }

  increment() {
    // console.log(this)
    this.setState({counter: this.state.counter + 1})
  }

  render () {
    return <span>
      {this.state.counter}
      <button onClick={this.increment} style={{margin: '10px'}}>Increase</button>
    </span>
  }
}

export default Binding
