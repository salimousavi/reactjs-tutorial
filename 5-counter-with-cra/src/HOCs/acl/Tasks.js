import React from 'react'
import Button from './Button'
import ACL from './ACL'

class Tasks extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      tasks: [
        {id: 1, task: 'Dish washing'},
        {id: 2, task: 'Go to car wash'},
      ]
    }
  }

  render () {
    return <span>
      <Button permission="add_task">Add task</Button>
      <ol>
        {this.state.tasks.map(el => <li key={el.id}>{el.task} <Button permission="remove_task" > remove task </Button></li>)}
      </ol>
    </span>
  }
}

export default ACL(Tasks)
