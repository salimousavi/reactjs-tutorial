import React from 'react'
const user = {
  name: 'Ali',
  family: 'Mousavi',
  permissions: ['view_task_list', 'add_task']
}

function ACL(Component) {
  return class extends React.Component {
    render () {
      const {permission} = this.props

      // if (!user.permissions.includes(permission)) {
      //   return null
      // }

      return user.permissions.includes(permission) && <Component {...this.props} />
    }
  }
}

export default ACL