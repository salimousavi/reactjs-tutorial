import React from 'react'
import Tasks from './Tasks'

class App extends React.Component {
  render () {
    return <Tasks permission="view_task_list" />
  }
}

export default App
