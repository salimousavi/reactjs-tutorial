import React from 'react'

class LifeCycles extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      counter: 0
    }
    console.log('Constructor')
  }

  componentDidMount () {
    console.log('componentDidMount')
    setTimeout(() => this.setState({counter: 1}), 1000)
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    console.log('componentDidUpdate')
  }

  componentDidCatch (error, errorInfo) {
    console.log('componentDidCatch')
  }

  shouldComponentUpdate (nextProps, nextState, nextContext) {
    console.log('shouldComponentUpdate')
    return true
  }

  componentWillUnmount () {
    console.log('componentWillUnmount')
  }

  getSnapshotBeforeUpdate (prevProps, prevState) {
    console.log('getSnapshotBeforeUpdate')
    return null
  }

  static getDerivedStateFromProps(props, state) {
    console.log('getDerivedStateFromProps')
    console.log(props)
    console.log(state)
    // return null
    return {counter: 12}
  }

  render () {
    console.log('render')
    return <span>
      {this.state.counter}
    </span>
  }
}

export default LifeCycles
