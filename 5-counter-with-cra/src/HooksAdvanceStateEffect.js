import { useEffect, useState } from 'react'

function Counter () {
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    const intervalId = setInterval(() => setCounter(counter => counter + 1), 1000)

    return () => {
      clearInterval(intervalId)
      console.log('Clear side Effect and Unmount')
    }
  }, [])

  return counter
}

export default Counter
