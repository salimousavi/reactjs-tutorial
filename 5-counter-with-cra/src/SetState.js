import React from 'react'

class Tasks extends React.Component {
  constructor (props) {
    super(props);
    this.state = {
      tasks: [],
      count: 0
    }
  }

  generateTask () {
    const time = (new Date()).getTime()
    return {
      task: `Task ${time}`,
      id: time
    }
  }

  addTask () {
    this.setState({tasks: [...this.state.tasks, this.generateTask()]})
    if (this.props.calculateCount) {
      this.setState( state => ({ count: state.tasks.length }))
      // this.setState( { count: this.state.tasks.length })
    }
  }

  render () {
    return (
      <div>
        <button onClick={() => this.addTask()}>Add task</button>
        <hr/>
        count: {this.state.count}
        <hr/>
        <ul>
          {this.state.tasks.map(el => <li key={el.id}> {el.task} </li>)}
        </ul>
      </div>
    );
  }

}

export default Tasks
