import React from 'react'

class Form extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }

    this.handleChangeUsername = this.handleChangeUsername.bind(this)
    this.handleChangePassword = this.handleChangePassword.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChangeUsername(event) {
    this.setState({username: event.target.value})
  }

  handleChangePassword(event) {
    this.setState({password: event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault()
    console.log(this.state);
  }

  render () {
    return <form onSubmit={this.handleSubmit}>
      <label>
        username:
        <input type="text" value={this.state.username} onChange={this.handleChangeUsername} />
      </label>
      <label>
        password:
        <input type="password" value={this.state.password} onChange={this.handleChangePassword} />
      </label>
      <input type="submit" value="Submit" />
    </form>
  }
}

class AdvancedForm extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: ''
    }

    this.handleChangeInput = this.handleChangeInput.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChangeInput(event) {
    this.setState({[event.target.name]: event.target.value})
  }

  handleSubmit(event) {
    event.preventDefault()
    console.log(this.state);
  }

  render () {
    return <form onSubmit={this.handleSubmit}>
      <label>
        username:
        <input name="username" type="text" value={this.state.username} onChange={this.handleChangeInput} />
      </label>
      <label>
        password:
        <input name="password" type="password" value={this.state.password} onChange={this.handleChangeInput} />
      </label>
      <input type="submit" value="Submit" />
    </form>
  }
}

export default AdvancedForm
