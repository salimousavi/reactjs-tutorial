import React from 'react'

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      users: [
        {id: 1, name: 'ali'},
        {id: 2, name: 'eli'},
        {id: 3, name: 'qoli'},
      ]
    }
  }

  render () {
    return <div>
      {this.state.users.map(el => <User key={el.id} user={el} />)}
    </div>
  }
}

function User ({user}) {
  return (
    <div>
      <span>Name: </span>
      <span>{user.name}</span>
    </div>
  )
}

export default App
