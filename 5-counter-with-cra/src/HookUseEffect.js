import React, {useState, useEffect} from 'react'

function App() {
  const [show, setShow] = useState(true)

  return (
    <div>
      <div onClick={() => setShow(!show)}>
        {show ? 'hide' : 'show'}
      </div>
      {show && <UseEffect />}
    </div>

  )
}

function UseEffect() {
  const [counter, setCounter] = useState(0)
  const [users, setUsers] = useState([])

  useEffect(() => {
    console.log('Log every changes')
  })

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    fetch('https://jsonplaceholder.typicode.com/users', {signal})
      .then(res => res.json())
      .then(result => setUsers(result))

    return () => {
      controller.abort()
    }
  }, [])

  useEffect(() => {
    const timeoutId = setTimeout(() => setCounter(counter + 1), 1000)

    return () => {
      clearTimeout(timeoutId)
      console.log('Clear side Effect')
    }
  }, [counter])

  return <div>
    {counter}
    <hr/>
    <ul>
      {users.map(el => <li key={el.id}>{el.name}</li>)}
    </ul>
  </div>
}

export default App