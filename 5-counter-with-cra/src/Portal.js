import React, {useEffect, useState} from 'react'
import ReactDOM from 'react-dom'

const portal = document.getElementById('portal')

// function Portal() {
//     const [counter, setCounter] = useState(0)
//
//     useEffect(() => {
//         setInterval(() => setCounter(counter => counter + 1), 1000)
//     }, [])
//
//     return ReactDOM.createPortal(counter, portal)
// }

class Portal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            counter: 0
        }
    }

    componentDidMount() {
        setInterval(() => this.setState({counter: this.state.counter + 1}), 1000)
    }

    render() {
        return ReactDOM.createPortal(this.state.counter, portal)
    }
}

export default Portal
