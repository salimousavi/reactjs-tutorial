import React, { useState, useEffect, useMemo } from 'react'

function App() {
  const [show, setShow] = useState(true)

  return (
    <div>
      {show && <UseMemo />}
      <div onClick={() => setShow(!show)}>
        show
      </div>
    </div>

  )
}

function UseMemo() {
  const [counter, setCounter] = useState(0)

  useEffect(() => {
    const intervalId = setInterval(() => setCounter(counter => counter + 1), 1000)

    return () => clearInterval(intervalId)
  }, [])

  const myExpensiveValue = useMemo(() => {
    return (9999n ** 99999n).toString()
  }, [])

  return <div>
    <span>{counter}</span>
    <hr/>
    <span>{myExpensiveValue}</span>
  </div>
}

export default App