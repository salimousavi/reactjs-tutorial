import React from 'react'

class CreateRef extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      showSearchBox: false
    }

    this.textInput = React.createRef()
  }

  showSearchBox () {
    this.setState({showSearchBox: true}, () => this.textInput.current.focus())
    // this.textInput.current.focus()
  }

  render () {
    return <span>
      <input style={{display: this.state.showSearchBox ? 'inline' : 'none'}} type="text" ref={this.textInput}
             placeholder="Search"/>
      <button onClick={() => this.showSearchBox()}>Search</button>
    </span>
  }
}

export default CreateRef
