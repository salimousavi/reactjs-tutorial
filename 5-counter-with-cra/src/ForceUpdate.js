import React from 'react'

class ForceUpdate extends React.Component {

  getRandom() {
    this.forceUpdate()
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("componentDidUpdate");
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    console.log("shouldComponentUpdate");
  }

  render () {
    console.log("render");
    return <span>
      {Math.random()}
      <button onClick={this.getRandom.bind(this)}>Get random</button>
    </span>
  }
}

export default ForceUpdate
