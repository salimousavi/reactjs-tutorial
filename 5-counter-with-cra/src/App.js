import React from 'react'

class App extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      counter: 0
    }
  }

  render () {
    return <span>
      {this.state.counter}
      <button onClick={() => this.setState({counter: this.state.counter + 1})} style={{margin: '10px'}}>Increase</button>
      <button onClick={() => this.setState({counter: this.state.counter - 1})}>Decrease</button>
    </span>
  }
}

export default App
