import React, {useState} from 'react'

function App() {
  const [todos, setTodos] = useState([])
  const [task, setTask] = useState('')

  function addTodo() {
    setTodos(todos => [...todos, {id: todos.length ? todos[todos.length - 1].id + 1 : 1, title: task}])
    setTask('')
  }

  function removeTodo(id) {
    setTodos(todos => todos.filter(todo => todo.id !== id))
  }

  return (
    <div>
      <ul>
        {todos.map(el =>
          <li key={el.id}>
            {el.title}
            <span onClick={() => removeTodo(el.id)} style={{color: 'red', padding: '10px'}}>x</span>
          </li>)}
      </ul>
      <input type="text" value={task} onChange={e => setTask(e.target.value)}/>
      <button onClick={() => addTodo()}>Add</button>
    </div>

  )
}

export default App
