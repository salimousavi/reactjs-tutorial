import React from 'react'

class App extends React.PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      counter: 0,
      status: 0
    }
  }

  componentDidUpdate (prevProps, prevState, snapshot) {
    console.log('App did update')
  }

  render () {
    return <span>
      <div>counter: {this.state.counter}</div>
      <div>status: {this.state.status}</div>
      <MyComponent counter={this.state.counter} />
      <MyPureComponent counter={this.state.counter} />
      <button onClick={() => this.setState({counter: this.state.counter + 1})} style={{margin: '10px'}}>Increase counter</button>
      <button onClick={() => this.setState({status: this.state.status + 1})}>Increase status</button>
    </span>
  }
}

class MyComponent extends React.Component {

  // shouldComponentUpdate (nextProps, nextState, nextContext) {
  //   if (nextProps.counter === this.props.counter) {
  //     return false
  //   }
  //   return true
  // }

  render () {
    console.log('MyComponent render: ', this.props.counter)
    return <div>Component {this.props.counter}</div>
  }
}

class MyPureComponent extends React.PureComponent {

  render () {
    console.log('MyPureComponent render', this.props.counter)
    return <div>PureComponent {this.props.counter}</div>
  }
}

export default App
