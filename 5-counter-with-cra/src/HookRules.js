import React, {useState} from 'react'

function App() {


  // Don't call Hooks inside loops, condition and nested Functions
  // Hooks only use in Functional component and custom hooks

  // function func() {
  //   const [count, setCount] = useState(0)
  // }

  if (true) {
    const [count, setCount] = useState(0)
  }

  for (let i = 0; i < 5; i++) {
    const [count, setCount] = useState(0)
  }

  return (
    <div>
      {count}
      <hr/>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setCount(count - 1)}>-</button>
    </div>

  )
}

export default App
