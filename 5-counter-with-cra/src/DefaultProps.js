import React from 'react'

class Counter extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            counter: props.counter
        }
    }

    render() {
        return <button style={{color: this.props.color}} onClick={() => this.setState({counter: this.state.counter + 1})}>
            {this.state.counter}
        </button>
    }
}

Counter.defaultProps = {
    counter: 1,
    color: 'red'
}

class App extends React.Component {

    render() {
        return (
            <React.Fragment>
                <Counter/>
                <hr/>
                <Counter counter={10} color="green"/>
            </React.Fragment>
        )
    }
}


export default App
