import React from 'react'

class ErrorBoundaries extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            hasError: false
        }
    }

    static getDerivedStateFromError(error) {
        console.log("My Error", error);
        // return { hasError: true };
    }

    // Will deprecated
    componentDidCatch(error, errorInfo) {
        console.log("componentDidCatch", error, errorInfo);
        this.setState({ hasError: true })
    }

    render() {
        if (this.state.hasError) {
            return 'Crashed'
        }

        return this.props.children
    }
}

class Child extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            counter: 0
        }
    }

    render() {
        if (this.state.counter === 4 ){
            throw new Error('Crashed app')
        }
        // throw new Error('SALAM')

        return <button onClick={() => this.setState({counter: this.state.counter + 1})}>{this.state.counter}</button>
    }
}

class App extends React.Component {

    render() {
        return (
            <ErrorBoundaries>
              <Child/>
            </ErrorBoundaries>
        )
    }
}


export default App
