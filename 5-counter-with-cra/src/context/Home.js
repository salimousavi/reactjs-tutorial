import React, { useContext } from 'react'
import ThemeContext from './ThemeContext'

function Home () {
  const theme = useContext(ThemeContext)

  return (
    <article style={{flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <h1>My Home Page</h1>
      <p>Duo legere menandri tacimates an, quo esse iudicabit eu, eos erat tamquam imperdiet et. Ex nec probo clita. Nemore adipisci ne est, eos deleniti salutatus maiestatis ex. Cu usu quis inani tibique. Duis animal pertinax id duo. Mea reque prompta ad. Reque deseruisse scribentur ius ad, id usu exerci fuisset.
      </p>
      <p>
        At lorem interesset vel, veri paulo laboramus ex cum. An minimum nominavi invidunt sea. An dicant consulatu eos. Ut legimus albucius sea.
      </p>
    </article>
  )
}

export default Home
