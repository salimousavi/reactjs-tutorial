import React, { useContext } from 'react'
import { Redirect, Route, useLocation } from 'react-router-dom'
import UserContext from './UserContext'

function PrivateRoute ({children, ...props}) {
  const {authenticated} = useContext(UserContext)
  const location = useLocation()

  if (authenticated) {
    return <Route {...props}>{children}</Route>
  }
  else {
    return <Redirect to={{pathname: '/login', state: {oldPath: location.pathname}}} />
  }
}

export default PrivateRoute