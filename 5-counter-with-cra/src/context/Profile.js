import React, { useContext } from 'react'
import ThemeContext from './ThemeContext'
import { Link } from 'react-router-dom'
import UserContext from './UserContext'

function Home () {
  const theme = useContext(ThemeContext)
  const {user} = useContext(UserContext)

  return (
    <article style={{flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <h1>My profile</h1>
      <p>
        My name is {user.name}
      </p>
    </article>
  )
}

export default Home
