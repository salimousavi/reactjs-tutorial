import React from 'react'
import ThemeContext from './ThemeContext'
import { Link, NavLink } from 'react-router-dom'
import './sidebar.css'

class Sidebar extends React.Component {

  static contextType = ThemeContext

  render () {
    return (
      <aside style={{flex: 1, border: 'solid 1px', padding: '1em', margin: '1em', ...this.context}}>
        <h4>My Sidebar</h4>

        <ul>
          <li>
            <NavLink exact to="/"> Home </NavLink>
          </li>
          <li>
            <NavLink to="/post/1"> Post 1 </NavLink>
          </li>
          <li>
            <NavLink to="/post/2"> Post 2 </NavLink>
          </li>
          <li>
            <NavLink to="/post/3"> Post 3 </NavLink>
          </li>
          <li>
            <NavLink to="/post/4"> Post 4 </NavLink>
          </li>
          <li>
            <NavLink to="/profile"> Profile </NavLink>
          </li>
        </ul>
      </aside>
    )
  }
}

export default Sidebar
