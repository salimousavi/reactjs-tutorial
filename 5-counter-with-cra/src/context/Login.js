import React, { useContext } from 'react'
import { useHistory, Redirect, useLocation } from 'react-router-dom'
import UserContext from './UserContext'
import ThemeContext from './ThemeContext'

function Login () {
  const {login, authenticated} = useContext(UserContext)
  const theme = useContext(ThemeContext)

  const history = useHistory()
  const location = useLocation()

  function singIn () {
    const { oldPath } = location.state || {oldPath: '/'}

    login()
    history.push(oldPath)
  }

  if (authenticated) {
    return <Redirect to="/"/>
  }

  return (
    <article style={{flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <button onClick={() => singIn()}>Login</button>
    </article>
  )
}

export default Login