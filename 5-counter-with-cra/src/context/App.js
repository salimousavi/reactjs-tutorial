import React, { useEffect, useState } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import cookie from 'js-cookie'
import Header from './Header'
import Home from './Home'
import Post from './Post'
import Login from './Login'
import NotFound from './NotFound'
import Profile from './Profile'
import PrivateRoute from './PrivateRoute'
import Sidebar from './Sidebar'
import Footer from './Footer'
import ThemeContext, { themes } from './ThemeContext'
import UserContext from './UserContext'

function App () {

  const [theme, setTheme] = useState(themes.light)
  const [loading, setLoading] = useState(true)
  const [user, setUser] = useState({})
  const [authenticated, setAuthenticated] = useState(false)

  // useEffect(() => {
  //   fetch('https://jsonplaceholder.typicode.com/users/1')
  //     .then(res => res.json())
  //     .then(result => setUser(result))
  // }, [])

  useEffect(() => {
    // if (cookie.get('token')) {
    //   login()
    // }
    // else {
    //   setLoading(false)
    // }
    cookie.get('token') ? login() : setLoading(false)
  }, [])

  function login () {
    setLoading(true)
    fetch('https://jsonplaceholder.typicode.com/users/1')
      .then(res => res.json())
      .then(result => {
        cookie.set('token', 'my-token', { expires: 7 })
        setUser(result)
        setAuthenticated(true)
        setLoading(false)
      })
  }

  function logout () {
    cookie.remove('token')
    setUser({})
    setAuthenticated(false)
  }

  if (loading) {
    return 'loading ...'
  }

  return (
    <div>
      <BrowserRouter>
        <UserContext.Provider value={{user, authenticated, login, logout}}>
          <ThemeContext.Provider value={theme}>
            <div>
              {theme === themes.light ?
                <button onClick={() => setTheme(themes.dark)}>Dark</button> :
                <button onClick={() => setTheme(themes.light)}>Light</button>
              }
            </div>
            <Header/>
            <section style={{display: 'flex'}}>
              <Sidebar/>
              <Switch>
                <Route path="/" exact component={Home}/>
                <Route path="/post/:id" component={Post}/>
                <Route path="/login" component={Login}/>
                <PrivateRoute path="/profile" component={Profile}/>
                <Route path="*" component={NotFound}/>
              </Switch>
              {/*<Home/>*/}
            </section>
            <Footer/>

          </ThemeContext.Provider>
        </UserContext.Provider>
      </BrowserRouter>
    </div>
  )
}

export default App