import React from 'react'
import ThemeContext from './ThemeContext'

class Footer extends React.Component {
  render () {
    return (
      <footer style={{border: 'solid 1px', padding: '1em', margin: '1em', ...this.context}}>
        <h2>My Footer</h2>
      </footer>
    )
  }
}

Footer.contextType = ThemeContext

export default Footer
