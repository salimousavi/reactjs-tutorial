import React, { useContext } from 'react'
import ThemeContext from './ThemeContext'
import { Link } from 'react-router-dom'

function Home () {
  const theme = useContext(ThemeContext)

  return (
    <article style={{flex: 3, border: 'solid 1px', padding: '1em', margin: '1em', ...theme}}>
      <h1>NotFound Page</h1>
      <p>
        Go to home page => <Link to="/" >Home</Link>
      </p>
    </article>
  )
}

export default Home
