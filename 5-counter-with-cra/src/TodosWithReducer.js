import React, {useState, useReducer} from 'react'

function reducer(state, action) {
  switch (action.type) {
    case 'ADD_TODO':
      return [...state, {id: state.length ? state[state.length - 1].id + 1 : 1, title: action.payload}]

    case 'REMOVE_TODO':
      return state.filter(todo => todo.id !== action.payload)

    default:
      throw new Error('Action type not valid')
  }
}

function App() {
  const [state, dispatch] = useReducer(reducer, [])
  const [task, setTask] = useState('')

  function addTodo() {
    dispatch({type: 'ADD_TODO', payload: task})
    setTask('')
  }

  function removeTodo(id) {
    dispatch({type: 'REMOVE_TODO', payload: id})
  }

  return (
    <div>
      <ul>
        {state.map(el =>
          <li key={el.id}>
            {el.title}
            <span onClick={() => removeTodo(el.id)} style={{color: 'red', padding: '10px'}}>x</span>
          </li>)}
      </ul>
      <input type="text" value={task} onChange={e => setTask(e.target.value)}/>
      <button onClick={() => addTodo()}>Add</button>
    </div>

  )
}

export default App
