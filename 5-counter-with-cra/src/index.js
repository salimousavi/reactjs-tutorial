import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
// import LifeCycles from './LifeCycles';
// import StrictModeError from './StrictModeError';
// import ErrorBoundaries from './ErrorBoundaries';
// import DefaultProps from './DefaultProps';
// import ForceUpdate from './ForceUpdate';
// import Binding from './Binding';
// import Portal from './Portal';
// import Form from './Form';
// import CreateRef from './CreateRef';
// import SetState from './SetState';
// import Stateless from './Stateless';
// import UseEffect from './HookUseEffect';
// import HookRules from './HookRules';
// import HookCustom from './HookCustom';
// import TodosWithState from './TodosWithState';
// import HookUseReducer from './HookUseReducer';
// import TodosWithReducer from './TodosWithReducer';
// import HookUseMemo from './HookUseMemo';
import HookUseCallback from './HookUseCallback';
// import HookUseRef from './HookUseRef';
// import HookUseState from './HookUseState';
// import ClassVsFunctionComponent from './ClassVsFunctionComponent';
// import UseMemo from './UseMemo';
import ContextApp from './context/App';
// import PureComponent from './PureComponent';
// import App from './HOCs/acl/App';
// import AdvanceStateEffect from './AdvanceStateEffect';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(
  // <React.StrictMode>
  //   <App />
  //   <LifeCycles />
  //   <StrictModeError />
  //   <ErrorBoundaries />
  //   <DefaultProps />
  //   <ForceUpdate />
  // {/*//   <AdvanceStateEffect />*/}
  //   <Portal />
  //   <Form />
  //   <PureComponent />
  //   <SetState calculateCount={true}/>
  //   <Stateless />
  //   <UseEffect />
  //   <UseMemo />
    <ContextApp />
  //   <HookUseState />
  //   <UseEffect />
  //   <HookRules />
  //   <HookCustom />
  //   <TodosWithState />
  //   <HookUseReducer />
  //   <TodosWithReducer />
  //   <HookUseMemo />
  //   <HookUseCallback />
    // <HookUseRef />
  // <hr/>
    // <ClassVsFunctionComponent />
    // <CreateRef />
  // {/*  // /!*<Binding />*!/*/}
  // </React.StrictMode>
,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
