import React from 'react'

class Child extends React.Component {
  state = {
    counter: 1
  }

  componentDidMount () {
    console.log("componentDidMount")
    this.intervalID = setInterval(() => this.setState({counter: this.state.counter + 1}), 1000)
  }

  componentWillUnmount () {
    console.log("componentWillUnmount")
    console.log(this.intervalID)
    clearInterval(this.intervalID)
  }

  render () {
    return <span>CHild {this.state.counter}</span>
  }
}


class UnMounting extends React.Component {
  state = {
    mount: true
  }

  render () {
    return <>
      {this.state.mount && <Child />}
      <hr />
      <button onClick={() => this.setState({mount: !this.state.mount})}>Unmounting</button>
    </>
  }
}

export default UnMounting