import React, { useState, useCallback } from 'react'

function App() {
  const [height, setHeight] = useState(0);
  const [counter, setCounter] = useState(0);

  // Adding a Ref to a DOM Element
  // React supports a special attribute that you can attach to any component.
  // The ref attribute takes a callback function, and the callback will be executed immediately
  // after the component is mounted or unmounted.

  // React will call the ref callback with the DOM element when the component mounts,
  // and call it with null when it unmounts.
  // https://stackoverflow.com/questions/44721779/how-does-the-ref-callback-work-in-react

  const heightRef = useCallback(node => {
    console.log(node)
    if (node !== null) {
      setHeight(node.getBoundingClientRect().height);
    }
  }, []);


  const focusRef = useCallback(node => {
    console.log(node)
    node && node.focus()
  }, [])

  // Incorrect
  // const focusRef = node => {
  //   console.log(node)
  //   node && node.focus()
  // }

  return (
    <>
      <input type="text" ref={focusRef}/>
      <h1 ref={heightRef}>Hello, world</h1>
      <h2>The above header is {Math.round(height)}px tall</h2>
      <div onClick={() => setCounter(counter => counter +1)}>{counter}</div>
    </>
  );
}

export default App
