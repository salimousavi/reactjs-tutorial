import React, { useState } from 'react'

function CounterFunction () {

  const [count, setCount] = useState(0)

  return (
    <div>
      {count}
      <button onClick={() => setCount(count + 1)}>+</button>
    </div>
  )
}

class CounterClass extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      count: 0
    }
  }

  render () {
    return (
      <div>
        {this.state.count}
        <button onClick={() => this.setState({count: this.state.count + 1})}>+</button>
      </div>
    )
  }
}

// export default CounterClass
export default CounterFunction
