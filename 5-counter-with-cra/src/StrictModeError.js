import React from 'react'

class StrictModeError extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      counter: 0
    }
    console.log('Constructor')
  }

  UNSAFE_componentWillMount() {
    console.log("UNSAFE_componentWillMount");
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    console.log('UNSAFE_componentWillReceiveProps');
  }

  UNSAFE_componentWillUpdate(nextProps, nextState, nextContext) {
    console.log("UNSAFE_componentWillUpdate");
  }

  render () {
    console.log('render')
    return <span>
      {this.state.counter}
    </span>
  }
}

export default StrictModeError
