const root = $('#root')

const counter = $('<span>0</span>')

const increaseButton = $('<Button>Increase</Button>').css('margin', '10px').click(() => counter.text(+counter.text() + 1))
const decreaseButton = $('<Button>deccrease</Button>').click(() => counter.text(+counter.text() - 1))

root.append(counter)
root.append(increaseButton)
root.append(decreaseButton)