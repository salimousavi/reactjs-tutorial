import React from 'react'
import ReactDOm from 'react-dom'
import {render} from '@testing-library/react'

import Counter from './Counter'

test("counter testing pure", () => {
  const root = document.createElement('div')
  ReactDOm.render(<Counter />, root)

  const h1 = root.querySelector('h1').textContent
  expect(h1).toBe('My counter')

  const button = root.querySelector('button').textContent
  expect(button).toBe('Increase')
})

test("counter testing testing library", () => {
  const {getByText, getByTestId} = render(<Counter />)
  getByText('My counter')
  getByText('Increase')
  expect(getByTestId('counter-value').textContent).toBe("0")
})

test("counter testing click", () => {
  const {getByText, getByTestId} = render(<Counter />)
  const button = getByText("Increase")

  const value = getByTestId("counter-value")
  expect(value.textContent).toBe("0")

  button.click()
  button.click()
  button.click()
  button.click()
  expect(value.textContent).toBe("4")
  button.click()
  button.click()
  expect(value.textContent).toBe("6")

})









// import React from 'react'
// import { render, fireEvent, act, wait, screen } from '@testing-library/react'
// import Counter from './Counter'
//
// it('Test Increase button', () => {
//   const r = {}
//   act(() => render(<Counter />, r))
//   // const r = render(<Counter />)
//   // console.log(r)
//   const {debug, findByText, getByText, getByTestId} = r
//   const button = getByText('Increase');
//   // const counter = getByTestId('counter-value');
//   const counter2 = getByText(0);
//   // act(() => fireEvent.click(button))
//   // button.click()
//
//   console.log(counter2.innerText)
//   // expect(counter.innerText).toBe(2)
//
//
// })