import React, { useState } from 'react'

function Counter() {

  const [counter, setCounter] = useState(0)


  return (
    <div>
      <h1>My counter</h1>
      <span data-testid="counter-value">{counter}</span>
      <button onClick={() =>setCounter(counter => counter + 1)}>Increase</button>
    </div>
  )
}

export default Counter