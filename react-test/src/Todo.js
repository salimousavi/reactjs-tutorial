import React, { useEffect, useState } from 'react'

function Todo() {

  const [todo, setTodo] = useState({})

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/todos/1')
      .then(response => response.json())
      .then(json => setTodo(json))
  }, [])

  return (
    <div>
      <h1>Todo Item</h1>
      <p>{todo.title}</p>
    </div>
  )
}

export default Todo