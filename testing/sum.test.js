const {sum: mySum, sumString: mySumString} = require('./sum.js')

const sum = jest.fn(mySum)
const sumString = jest.fn(mySumString)

// if (sum(1,2) !== 3) {
//   throw 'Test fail'
// }

// beforeAll(() => {
//   console.log("Before execute all test")
// })
//
// beforeEach(() => {
//   console.log("Before execute every test")
// })
//
// afterAll(() => {
//   console.log("After execute all test")
// })
//
// beforeEach(() => {
//   console.log("After execute every test")
// })

test('Testing', () => {
  expect(false).toBeTruthy()
  expect(false).toBeFalsy()
})

describe('Summer tests', () => {
  test('Testing sum', () => {
    expect(sum(1, 2)).toBe(3)
    expect(sum(5, 2)).toBe(7)
    expect(sum(5, 7)).toBe(12)
    expect(sum(5, NaN)).toBeNaN()
  })

  test('Testing sum string', () => {
    expect(sumString(1, 2)).toBe('Sum is 3')
    expect(sumString(1, 5)).toBe('Sum is 6')
    expect(sumString).toBeCalledTimes(2)
    expect(sumString).toBeCalledWith(1,2)
    expect(sumString).toBeCalledWith(1,5)
  })
})

