import React from 'react';
import { Layout } from 'antd'
import { Route, Switch } from 'react-router-dom'
import Sidebar from './generic/Sidebar'
import Header from './generic/Header'
import Footer from './generic/Footer'
import NotFound from './generic/NotFound'
import Login from './generic/Login'
import PersonRouter from './person/Router'
import PostRouter from './post/Router'
import 'antd/dist/antd.css'
import '../assets/css/general.css'
import Dashboard from "./generic/Dashboard";
import { useSelector } from 'react-redux'

const {Header: AntHeader, Footer: AntFooter, Sider, Content} = Layout;

function App () {

  const loading = useSelector(state => state.userIsLoading)
  const isLoggedIn = useSelector(state => state.isLoggedIn)

  // if (loading) {
  //   return 'Loading ....'
  // }

  if (!isLoggedIn) {
    return <Login />
  }

  return (
    <Layout>
      <AntHeader>
        <Header/>
      </AntHeader>
      <Layout>
        <Sider><Sidebar/></Sider>
        <Content style={{padding: '50px'}}>
          <Switch>
            <Route exact path="/" component={Dashboard}/>
            <Route path="/person" component={PersonRouter}/>
            <Route path="/post" component={PostRouter}/>
            <Route path="*" component={NotFound}/>
          </Switch>
        </Content>
      </Layout>
      <AntFooter>
        <Footer/>
      </AntFooter>
    </Layout>
  )
}

export default App;
