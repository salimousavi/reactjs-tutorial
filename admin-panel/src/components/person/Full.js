import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { Link, useParams } from 'react-router-dom'
import axios from 'axios'
import { Row, Col } from 'antd'
import {person} from '../../redux/actions/person'

function Full () {
  const {id} = useParams()
  // const [item, setItem] = useState({})
  const dispatch = useDispatch()
  const item = useSelector(state => {
    console.log(state)
    return state.person
  })

  useEffect(() => {
    axios.get(`https://jsonplaceholder.typicode.com/users/${id}`)
      // .then(response => setItem(response.data))
      .then(response => dispatch(person(response.data)))
  }, [id])

  console.log(item);

  return (
    <div>
      <Row>
        <Col span={4}>نام: </Col>
        <Col span={20}>{item.name}</Col>
      </Row>
      <Row>
        <Col span={4}>نام کاربری: </Col>
        <Col span={20}>{item.username}</Col>
      </Row>
      <Row>
        <Col span={4}>ایمیل: </Col>
        <Col span={20}>{item.email}</Col>
      </Row>
      <Row>
        <Link to="/person">بازگشت به لیست</Link>
      </Row>
    </div>
  )
}

export default Full