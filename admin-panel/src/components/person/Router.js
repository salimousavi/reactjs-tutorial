import React from "react"
import {Route} from 'react-router-dom'
import List from './List'
import New from './New'
import Full from './Full'

function Router() {
    return <>
        <Route exact path="/person" component={List}/>
        <Route exact path="/person/new" component={New}/>
        <Route exact path="/person/:id/show" component={Full}/>
    </>
}

export default Router
