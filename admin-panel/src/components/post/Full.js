import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from 'react-redux'
import { Link, useParams } from 'react-router-dom'
import { Row, Col } from 'antd'
import { getItem } from '../../redux/actions/post'

function Full () {
  const {id} = useParams()
  const dispatch = useDispatch()
  const item = useSelector(state => state.post)

  useEffect(() => { dispatch(getItem(id)) }, [id])

  return (
    <div>
      <Row>
        <Col span={4}>عنوان: </Col>
        <Col span={20}>{item.title}</Col>
      </Row>
      <Row>
        <Link to="/post">بازگشت به لیست</Link>
      </Row>
    </div>
  )
}

export default Full