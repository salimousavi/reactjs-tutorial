import React from 'react';
import {connect} from 'react-redux'
import {getItems} from '../../redux/actions/post'
import { Table } from 'antd';
import {Link} from "react-router-dom";
import {EyeOutlined} from '@ant-design/icons'

const columns = [
  {
    title: 'عنوان',
    dataIndex: 'title',
    key: 'title',
  },
  {
    title: '',
    dataIndex: 'actions',
    key: 'actions',
    render: (field, record) => <Link to={`/post/${record.id}/show`}><EyeOutlined /></Link>
  }
];

class List extends React.Component {
  componentDidMount () {
    this.props.getItems()
  }

  render () {
    return (
      <Table columns={columns} dataSource={this.props.data} rowKey="id" />
    )
  };
}

const mapStateToProps = (state) => {
  return {
    data: state.posts,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getItems: (data) => dispatch(getItems(data)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(List)
