import React from "react"
import {Route} from 'react-router-dom'
import List from './List'
import New from './New'
import Full from './Full'

function Router() {
    return <>
        <Route exact path="/post" component={List}/>
        <Route exact path="/post/new" component={New}/>
        <Route exact path="/post/:id/show" component={Full}/>
    </>
}

export default Router
