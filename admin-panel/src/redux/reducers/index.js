import {combineReducers} from 'redux'
import {persons, person} from './person'
import {posts, post} from './post'
import {user, userIsLoading, isLoggedIn, loginSubmitLoading} from './user'

export default combineReducers({
  persons,
  person,
  posts,
  post,
  user,
  userIsLoading,
  isLoggedIn,
  loginSubmitLoading
})

