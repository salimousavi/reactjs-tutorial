import { createAction } from '@reduxjs/toolkit'
// import axios from 'axios'
import request from '../../tools/Request'

// export function posts (data) {
//   return {
//       type: 'POSTS',
//       payload: data
//   }
// }
//
// export function post (data) {
//   return {
//     type: 'POST',
//     payload: data
//   }
// }

// const posts = createAction('POSTS')
// const post = createAction('POST')
export const items = createAction('POSTS')
export const item = createAction('POST')

export function getItems () {
  return dispatch => request('/posts').then(response => dispatch(items(response.data)))
  // return (dispatch) => axios('https://jsonplaceholder.typicode.com/posts')
  //   .then(response => dispatch(posts(response.data)))
}

export function getItem (id) {
  return dispatch => request(`/posts/${id}`).then(response => dispatch(item(response.data)))
  // return (dispatch) => axios(`https://jsonplaceholder.typicode.com/posts/${id}`)
  //   .then(response => dispatch(post(response.data)))
}
