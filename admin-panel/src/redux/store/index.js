import {createStore, applyMiddleware} from 'redux'
// import thunk from 'redux-thunk'
import reducers from '../reducers'

const myMiddleware = store => next => action => {
  // console.log(store)
  // console.log(next)
  // console.log(action)
  if (typeof action === 'function') {
    return action(store.dispatch)
  }
  next(action)
}

// const myMiddleware = function (store) {
//   return function (next) {
//     return function (action) {
//       console.log(store)
//       console.log(next)
//       console.log(action)
//       next(action)
//     }
//   }
// }


const store = createStore(reducers,
  applyMiddleware(myMiddleware)
)

export default store

///////////////////// TOOLKIT

// import {configureStore} from '@reduxjs/toolkit'
// import reducers from '../reducers'
//
// const store = configureStore({
//   reducer: reducers
// })
//
// export default store